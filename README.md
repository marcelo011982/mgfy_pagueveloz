#Modulo Pagueveloz para emissão de boletos em magento 2.

##Instalação:

Inserir o conteudo do modulo na pasta app/code/Mgfy/Pagueveloz/

sudo bin/magento cache:clean

sudo bin/magento cache:flush

bin/magento setup:static-content:deploy -f

O campo 'taxvat' deve estar habilitado pois é dali que teremos o cpf do usuario para ser enviado ao Pagueveloz.

 


## 1) Para configurar deve-se ir em 'Stores->Configuration->Sales->Payment Methods->Mgfy Pagueveloz' .



Ali teremos: 

![alt_text](readme/pague_veloz_configuracao_admin.png)


Onde :

Enabled - Indica se o módulo está habilitado ou não;
Is in test Mode? - Verifica se o módulo está em homologação. Caso não estiver setado ele estará em produção;
Title - titulo a ser apresentado ao cliente no checkout;
How much days to pay the ticket (from the order date) - nú de dias corridos que serão adicionado para a data da compra indicando o vencimento do boleto;
Authentication User for API. - Usuário de autenticação da api;
Token for API. - Token de autenticação da API.

## 2) Fluxo e utilização:

Abaixo um video da utilização basica dele:
https://drive.google.com/file/d/1f9xgHIJMK5qsxhhgietBkL0ECvMkcfdG/view?usp=sharing


Existe um log de requisições no arquivo a var/log/payment.log . Abaixo o conteudo da requisição e do retorno de chamadas . Note que existe um campo chamaod 'SeuNumero' para enviar o id da compra feita no magento para controle :

https://drive.google.com/file/d/1pJiNAnJtWeelPqhfYOW1SmF6kUczPIgk/view?usp=sharing

## 3) Erros:


Em caso de erros de autenticação aparecera a tela :
![alt_text](readme/pagueveloz_error.png)
Nesses casos, há de se verificar se os dados de autenticação vistos em 1 estao corretos
Em caso da tella ![alt_text](readme/pague_veloz_error2.png) , verifique se o campo 'How much days to pay the ticket' está numérico e correto.
 


