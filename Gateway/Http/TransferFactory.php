<?php
namespace Mgfy\Pagueveloz\Gateway\Http;
use Magento\Payment\Gateway\Http\TransferBuilder;
use Magento\Payment\Gateway\Http\TransferFactoryInterface;
use Mgfy\Pagueveloz\Gateway\Converter\Converter;
use Mgfy\Pagueveloz\Gateway\Config;

class TransferFactory implements TransferFactoryInterface
{
    /**
     *
     * @var TransferBuilder 
     */
    private $transferBuilder;

    /**
     * @var Config
     */
    private $config;
    /**
     *
     * @var Converter 
     */
    private $converter;
    
    public function __construct(
        TransferBuilder $transferBuilder, 
        Converter $converter,
        Config $config
        )
    {
        $this->transferBuilder = $transferBuilder;
        $this->converter = $converter;
        $this->config = $config;
    }
    
    public function create(array $request)
    {
        return $this->transferBuilder
            ->setUri($this->config->getGatewayUrl())
            ->setMethod('POST')
            ->setBody($this->converter->convert($request))
            ->setHeaders($this->config->getGatewayHeaders())
            ->build();
    }
    
    
}
