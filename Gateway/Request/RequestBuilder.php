<?php

namespace Mgfy\Pagueveloz\Gateway\Request;

use Mgfy\Pagueveloz\Gateway\Config;
use Magento\Payment\Gateway\Request\BuilderInterface;
use Magento\Customer\Api\CustomerRepositoryInterface;
use DateTime;

class RequestBuilder implements BuilderInterface
{

    /**
     * @var Mgfy\Pagueveloz\Gateway\Config
     */
    private $config;

    /**
     * @var CustomerRepositoryInterface
     */
    private $customerInterface;


    /**
     * RequestBuilder constructor.
     * @param $config
     * @param $customer
     */

    public function __construct(
        Config $config,
        CustomerRepositoryInterface $customerInterface
    )
    {
        $this->config = $config;
        $this->customerInterface = $customerInterface;
    }

    public function build(array $buildSubject)
    {
        $paymentDataObject = $buildSubject['payment'];
        $order = $paymentDataObject->getOrder();
        $grandTotal = $order->getGrandTotalAmount();
        $daysToDeadline = $this->config->getDaysToDeadline();
        $deadLine = new DateTime(date("Y-m-d H:i:s"));
        $deadLine = $deadLine->add(new \DateInterval("P{$daysToDeadline}D"));
        $deadLine = $deadLine->format('d/m/Y');
        $customer = $this->customerInterface->getById($order->getCustomerId());
        return [
            'Sacado' => $customer->getFirstname() . " " . $customer->getLastname(),
            'CPFCNPJSacado' => $customer->getTaxvat(),
            'Vencimento' => $deadLine,
            'Valor' => $grandTotal,
            "Parcela" => 1,
            "SeuNumero" => $order->getOrderIncrementId(),
            "Linha1" => "sample string 5",
            "Linha2" => "samble string 6",
            "Email" => $customer->getEmail(),
            "DataEnvioEmail" => $deadLine,
            "Pdf" => true
        ];


    }
}
