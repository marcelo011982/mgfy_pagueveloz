<?php

namespace Mgfy\Pagueveloz\Gateway\Validator;

use Magento\Payment\Gateway\Validator\AbstractValidator;

class GeneralResponseValidator extends AbstractValidator
{
    public function validate(array $validationSubject)
    {
        $response = $validationSubject['response'];
        $isValid = true;
        $errorMessages = [];
        $errorCodes = [];

        foreach ($this->getResponseValidator($response) as $validator) {
            $responseValidator = $validator($response);
            if (!$responseValidator[0]) {
                $isValid = $responseValidator[0];
                $errorMessages = array_merge($errorMessages, $responseValidator[1]);
                $errorCodes = array_merge($errorMessages, $responseValidator[2]);
            }
        }
        return $this->createResult($isValid, $errorMessages, $errorCodes);
    }

    private function getResponseValidator($response)
    {
        return [
            function ($response) {
                return [
                    isset($response['Id']) && !empty($response['Id']),
                    [__("Authorization denied for gateway. Check Authorization user and token. Check also if 'How much days to pay the ticket' is set.")],
                    ['81703']
                ];
            },
            function ($response) {
                return [
                    isset($response['Url']) && !empty($response['Url']),
                    [__("Authorization denied for gateway. Check Authorization user and token. Check also if 'How much days to pay the ticket' is set.")],
                    ['81703']
                ];
            }

        ];
    }


}