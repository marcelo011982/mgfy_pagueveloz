<?php

namespace Mgfy\Pagueveloz\Gateway;


use Magento\Framework\Exception\NotFoundException;
use Magento\Payment\Gateway\Config\ValueHandlerPool;

class Config
{
    /**
     * @var ValueHandlerPool
     */
    private $valueHandlerPool;

    /**
     * Config constructor.
     * @param ValueHandlerPoolInterface $valueHandlerPoolInterface
     */
    public function __construct(ValueHandlerPool $valueHandlePool)
    {
        $this->valueHandlerPool = $valueHandlePool;
    }


    public function getGatewayUrl()
    {
        if ($this->isSandBox()) {
            return (string)$this->getValue('api_url_sandbox');
        }
        return (string)$this->getValue('api_url_production');
    }

    /**
     * @return bool
     */
    public function isSandBox()
    {
        return (bool)$this->getValue('is_sandbox');

    }

    /**
     * @return array
     */
    public function getGatewayHeaders()
    {

        return ['Content-Type' => 'application/json',
            'Authorization' => 'Basic ' . base64_encode("{$this->getApiLogin()}:{$this->getToken()}")];

    }

    /**
     * @return string
     */
    public function getApiLogin()
    {
        return (string) $this->getValue('api_login');
    }

    /**
     * @return string
     */
    public function getToken()
    {
        return (string) $this->getValue('token');
    }

    public function getDaysToDeadline()
    {
        return (string) $this->getValue('days_to_deadline');
    }


    private function getValue($field)
    {
        try {
            $handler = $this->valueHandlerPool->get($field);
            return $handler->handle(['field' => $field]);
        } catch (NotFoundException $exception) {
            return null;
        }

    }
}
