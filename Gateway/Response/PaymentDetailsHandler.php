<?php

namespace Mgfy\Pagueveloz\Gateway\Response;

use Magento\Payment\Gateway\Data\PaymentDataObjectInterface;
use Magento\Payment\Gateway\Response\HandlerInterface;
use Magento\Payment\Model\InfoInterface;

class PaymentDetailsHandler implements HandlerInterface
{
    /**
     * @var array
     */
    private $additionalInformation = [
        ResponseFields::ID => 'Id',
        ResponseFields::URL => 'Url'
    ];

    /**
     * @inheritDoc
     */
    public function handle(array $handlingSubject, array $response)
    {
        /** @var PaymentDataObjectInterface $paymentDataObject */
        $paymentDataObject = $handlingSubject['payment'];
        /** @var InfoInterface $payment */
        $payment = $paymentDataObject->getPayment();
        $transactionResponse = $response;
        foreach ($this->additionalInformation as $responseKey => $paymentKey) {
            if ((isset($transactionResponse[$responseKey])) && (!empty($transactionResponse[$responseKey]))) {
                $payment->setAdditionalInformation($paymentKey, $transactionResponse[$responseKey]);
            }
        }
    }

}