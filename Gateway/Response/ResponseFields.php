<?php

namespace Mgfy\Pagueveloz\Gateway\Response;

interface ResponseFields
{
    const ID = 'Id';
    const URL = 'Url';

}
