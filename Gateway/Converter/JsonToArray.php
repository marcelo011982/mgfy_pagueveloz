<?php
namespace Mgfy\Pagueveloz\Gateway\Converter;

use Magento\Payment\Gateway\Http\ConverterInterface;
use Magento\Framework\Serialize\SerializerInterface;



/**
 * Description of JsonToArray
 *
 * @author marcelo
 */
class JsonToArray implements ConverterInterface
{
    /**     
     * @var SerializerInterface
     */
    private $serializer;
    
    /**
     * 
     * @param SerializerInterface $serializer
     */
    public function __construct(
        SerializerInterface $serializer
    )
    {
        $this->serializer = $serializer;
        
    }

    /**
     * @inheritdoc
     */
    public function convert($response)
    {               
        return $this->serializer->unserialize($response);
    }
}
