<?php
namespace Mgfy\Pagueveloz\Gateway\Converter;
use Magento\Payment\Gateway\Http\ConverterInterface;

/**
 * Description of Converter
 *
 * @author marcelo
 */
class Converter
{
    /**
     *
     * @var ConverterInterface 
     */
    private $converter;
    
    /**
     * 
     * @param ConverterInterface $converter
     */
    
    public function __construct(
    ConverterInterface $converter
        )
    {
        $this->converter = $converter;
    }
    
    /**
     * 
     * @param array $request
     * @return array|string
     */
    
    public function convert(array $request) {
        return $this->converter->convert($request);
        
    }
}