<?php
namespace Mgfy\Pagueveloz\Gateway\Converter;
use Magento\Payment\Gateway\Http\ConverterInterface;
use Magento\Framework\Serialize\SerializerInterface;
/**
 * Description of ArrayToJson
 *
 * @author marcelo
 */
class ArrayToJson implements ConverterInterface
{
    
    /**
     * 
     * @param SerializerInterface $serializer
     */
    public function __construct(
        SerializerInterface $serializer
    )
    {
        $this->serializer = $serializer;
        
    }
    
    /**
     * @inheritdoc
     */
    public function convert($response)
    {               
        return $this->serializer->serialize($response);
    }
}
