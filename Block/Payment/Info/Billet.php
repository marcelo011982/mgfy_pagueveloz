<?php

namespace Mgfy\Pagueveloz\Block\Payment\Info;

use Magento\Payment\Block\ConfigurableInfo;

class Billet extends ConfigurableInfo
{
    const TEMPLATE = 'Mgfy_Pagueveloz::payment/info/billet.phtml';

    public function _construct()
    {
        $this->setTemplate(self::TEMPLATE);
        parent::_construct();

    }

    public function getBilletUrl()
    {
        return $this->getInfo()->getAdditionalInformation('Url');

    }

    public function getTitle()
    {
        return $this->getInfo()->getAdditionalInformation('method_title');
    }

}