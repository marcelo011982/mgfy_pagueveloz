/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */

/* @api */
define (
        [
            'uiComponent',
            'Magento_Checkout/js/model/payment/renderer-list'
        ],
        function (Component, rendererList) {
            'use strict';
            rendererList.push({
                type: 'mgfy_pagueveloz',
                component: 'Mgfy_Pagueveloz/js/view/payment/method-renderer/cc-form'
            });
            return Component.extend({});
        }
);